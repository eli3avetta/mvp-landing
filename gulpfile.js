const gulp = require('gulp');
const browserSync = require('browser-sync').create();
const reload = browserSync.reload;
const concat = require('gulp-concat');

const sass = require('gulp-sass');
const cleanCSS = require('gulp-clean-css');

const uglify = require('gulp-uglify');
const autoprefixer = require('gulp-autoprefixer');

const image = require('gulp-image');

const fileinclude = require('gulp-file-include');

// билд стилей
gulp.task('css', function (done) {
  gulp.src('app/styles.scss') // путь к папке со скриптами
      .pipe(sass.sync().on('error', sass.logError)) // работа с scss
      .pipe(autoprefixer({ cascade: false })) // префиксы для разных браузеров
      .pipe(cleanCSS({ compatibility: 'ie8' })) // очищение от пробелов и переносов
      .pipe(concat('styles.css')) // в какой файл объединить
      .pipe(gulp.dest('_dist'));

  // говорим об окончании операции
  done();
});

// билд js
gulp.task('js', function (done) {
  gulp.src('app/**/*.js') // путь к папке со скриптами
      .pipe(uglify()) // очищение от пробелов и переносов
      .pipe(concat('main.js')) // в какой файл объединить
      .pipe(gulp.dest('_dist'));

  // говорим об окончании операции
  done();
});

// перенос assets в директорию сервера
gulp.task('assets', function (done) {
  gulp.src('assets/**')
      .pipe(image())
      .pipe(gulp.dest('_dist/assets'));

  // говорим об окончании операции
  done();
});

// билд html
gulp.task('html', function(done) {
  gulp.src('app/index.html')
      .pipe(fileinclude({
        prefix: '@@',
        basepath: '@file'
      }))
      .pipe(gulp.dest('_dist'));

  // говорим об окончании операции
  done();
});

// запуск сервера, прослушек и билдов
gulp.task('watch', function () {
  // запуска сервера
  browserSync.init({
    server: {
      baseDir: "_dist" // от куда будут браться html файлы для севрера. Например index.html Будет доступен по адресу http://localhost:3000 или http://localhost:3000/index.html
    },
    // по дефолту http://localhost:3000, но можно менять
    // host: 'localhost',
    // port: 3000,
  });

  // запускаем прослушки на изменения файлов. В зависимости от типа запускаем билд и обновление страницы
  gulp.watch('app/**/*.scss', gulp.parallel('css')).on('change', reload);
  gulp.watch('app/**/*.js', gulp.parallel('js')).on('change', reload);
  gulp.watch('assets/**', gulp.parallel('assets')).on('change', reload);
  gulp.watch('app/**/*.html', gulp.parallel('html')).on('change', reload);
});

// в терминале: gulp (та, задача что default запуститься автоматически)
// сделает первую сборку и запустит сервер
gulp.task('default', gulp.parallel('css', 'js', 'assets', 'html', 'watch'));
